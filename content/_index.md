+++
path = "/"
title = "Latest posts"
sort_by = "date"
template = "section.html"

[extra]
header = {title = "Welcome to Switchbyte!", img = "$BASE_URL/img/me.jpg" }
section_path = "blog/_index.md"
max_posts = 4
+++

Switchbyte is your solution for cutting-edge web development and consultancy services. Our goal is to help your business thrive online with a website that's both visually stunning and highly functional. Let us know how we can help you take your website to the next level!