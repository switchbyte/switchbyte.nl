+++
title = "Almost no JavaScript"
date = 2023-01-06
updated = 2023-04-28
description = "JavaScript is only used when HTML and CSS aren't enough."

[taxonomies]
tags = ["showcase"]
+++

# JavaScript?

This theme has almost no JavaScript. It includes ~900 bytes of `.js` with the logic for the light/dark mode switch, which can be disabled by setting `theme_switcher = false` in the `config.toml` file.

KaTex support, which requires loading a 274 KB JavaScript file, can be activated for specific posts.

Other than that, it's a fast site with HTML and CSS. Just the way (most of) the web should be :-)
